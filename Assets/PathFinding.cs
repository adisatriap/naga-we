﻿using UnityEngine;
using System.Collections;

public class PathFinding : MonoBehaviour {

    public float health = 50f;

    public GameObject foodPrefabs;
    public Vector3 center;
    public Vector3 size;

    [SerializeField] Transform target;
    [SerializeField] float movementSpeed = 10f;
    [SerializeField] float rotationalDamp = .5f;

    [SerializeField] float rayCastOffset = 2.5f;
    [SerializeField] float DetectionDistance = 20f; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
       
        turn();
        Move();
        //PathFindings();
	}

    
    public void TakeDemage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();

        }
    }
       
    void Die() {

        Destroy(gameObject);

    }

    void Move() {
        transform.position += transform.forward * movementSpeed * Time.deltaTime;
    }

   
    
    void turn() {
        Vector3 pos = target.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(pos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);
    }

    void PathFindings() {
        Vector3 left = transform.position - transform.right * rayCastOffset;
        Vector3 right = transform.position + transform.right * rayCastOffset;
        Vector3 up = transform.position + transform.up * rayCastOffset;
        Vector3 down = transform.position - transform.up * rayCastOffset;
    }
}
