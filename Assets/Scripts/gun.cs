﻿using UnityEngine;
using System.Collections;

public class gun : MonoBehaviour {

	public float damage = 10f;
	public float range = 100f;

	public Camera fpsCam;

	// Update is called once per frame
	void Update () {
		
			shoot ();

	}

	void shoot(){
		RaycastHit hit;
		if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
		{
			Debug.Log(hit.transform.name);

			PathFinding targetna = hit.transform.GetComponent<PathFinding> ();
			if (targetna != null) {
				GameObject bullet = Instantiate (Resources.Load ("bullet", typeof(GameObject))) as GameObject;
				Rigidbody rb = bullet.GetComponent<Rigidbody> ();
				bullet.transform.rotation = Camera.main.transform.rotation;
				bullet.transform.position = Camera.main.transform.position;
				rb.AddForce (Camera.main.transform.forward * 500f);
				Destroy (bullet,1);

				targetna.TakeDemage (damage);
			} else {
				Debug.Log ("no target");
			}
		}
	}
}
