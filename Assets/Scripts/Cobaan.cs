﻿using UnityEngine;
using System.Collections;

public class Cobaan : MonoBehaviour
{

    public float health = 50f;

    private NavMeshAgent theAgent;
    public Transform targetnya;

    void Start()
    {
        theAgent = GetComponent<NavMeshAgent>();

    }

    void Update()
    {
        //theAgent.SetDestination(targetnya.position);
    }

    public void TakeDemage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();

        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
