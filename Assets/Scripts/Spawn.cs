﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Spawn : MonoBehaviour {

    public GameObject foodPrefabs;
    public Vector3 center;
    public Vector3 size;

    protected float Totalwaktu = 0f;
    //soon deleted
    public GameObject spawnParent;

    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        //if (Input.GetKey(KeyCode.Q))
        //{
        //    SpawnObjek();
        //}
        Totalwaktu += Time.deltaTime;
        if (Totalwaktu>3) {
            SpawnObjek();
        }
        //StartCoroutine(WaitForSpawn());
    }

    //private void OnDestroy()
    //{
    //    SpawnObjek();
    //}

    //public IEnumerator WaitForSpawn()
    //{
    //    while (true)
    //    {
    //        yield return new WaitForSeconds(3);
    //        SpawnObjek();
    //    }
    //}

    public void SpawnObjek() {
        
        Vector3 pos = new Vector3(Random.Range(-size.x/2, size.x/2), Random.Range(-size.y/2, size.y/2), Random.Range(-size.z / 2, size.z / 2));

        GameObject itemClone = Instantiate(foodPrefabs, pos, Quaternion.identity) as GameObject;
        itemClone.transform.parent = spawnParent.transform;
        Totalwaktu = 0f;

        //Instantiate(foodPrefabs, pos, Quaternion.identity);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center, size);
    }
}
