﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target : MonoBehaviour
{
    public float health = 50f;

    private Transform goal;
    private NavMeshAgent theAgent;
    public Transform targetnya;

    void Start()
    {
        theAgent = GetComponent<NavMeshAgent>();

    }

    void Update()
    {
        //theAgent.SetDestination(targetnya.position);
    }

    public void TakeDemage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();

        }
    }

    void Die()
    {
        Destroy(gameObject);
       
        //first disable the zombie's collider so multiple collisions cannot occur
        //GetComponent<CapsuleCollider>().enabled = false;

        //stop the zombie from moving forward by setting its destination to it's current position
        // theAgent.destination = gameObject.transform.position;


        //instantiate a new zombie
        GameObject box = Instantiate(Resources.Load("Capsule", typeof(GameObject))) as GameObject;

        //set the coordinates for a new vector 3
        float randomX = UnityEngine.Random.Range(-32f, 32f);
        float constantY = .01f;
        float randomZ = UnityEngine.Random.Range(-33f, 33f);
        //set the zombies position equal to these new coordinates
        box.transform.position = new Vector3(randomX, constantY, randomZ);

        theAgent = GetComponent<NavMeshAgent>();
        //if the zombie gets positioned less than or equal to 3 scene units away from the camera we won't be able to shoot it
        //so keep repositioning the zombie until it is greater than 3 scene units away. 
        while (Vector3.Distance(box.transform.position, Camera.main.transform.position) <= 3)
        {

            randomX = UnityEngine.Random.Range(-32f, 32f);
            randomZ = UnityEngine.Random.Range(-33f, 33f);

            box.transform.position = new Vector3(randomX, constantY, randomZ);

            theAgent = GetComponent<NavMeshAgent>();
        }
    }
  }